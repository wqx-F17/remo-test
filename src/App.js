import React from 'react';
import imageShow from './show.png';
import imageHide from './hide.png';
import ReactDOM from 'react-dom';
import './App.css';

class ShapeView extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      shape : props.shape,
      dragging: false,
      pos: {x: 0, y: 0},
      rel: null,
    }
  }

  componentDidUpdate(props, state) {
    if (this.state.dragging && !state.dragging) {
      document.addEventListener('mousemove', this.onMouseMove);
      document.addEventListener('mouseup', this.onMouseUp);
    } else if (!this.state.dragging && state.dragging) {
      document.removeEventListener('mousemove', this.onMouseMove);
      document.removeEventListener('mouseup', this.onMouseUp);
    }
  }

  onMouseDown = (e) => {
    if (e.button !== 0) return;
    let pos = ReactDOM.findDOMNode(this);
    this.setState({
      dragging: true,
      rel: {
        x: e.pageX - pos.offsetLeft,
        y: e.pageY - pos.offsetTop,
      }
    });
    e.stopPropagation();
    e.preventDefault();
  }
  onMouseUp = (e) => {
    this.setState({dragging: false});
    e.stopPropagation();
    e.preventDefault();
  }
  onMouseMove = (e) => {
    if (!this.state.dragging) return;
    this.setState({
      pos: {
        x: e.pageX - this.state.rel.x,
        y: e.pageY - this.state.rel.y
      }
    });
    this.props.onChangePosition(this.state.pos, this.state.shape.index);
    e.stopPropagation();
    e.preventDefault();
  }
  render () {
    return (
      <div
          style={{
            width : this.state.shape.position.width > 0 ? this.state.shape.position.width : -this.state.shape.position.width,
            height: this.state.shape.position.height > 0 ? this.state.shape.position.height : -this.state.shape.position.height, 
            borderWidth: 3, 
            borderColor : '#4ED081', 
            borderStyle:'dashed', 
            opacity: 1, 
            cursor: 'pointer',
            left: this.state.shape.position.width < 0 ? this.state.shape.position.start.x + this.state.shape.position.width : this.state.shape.position.start.x, 
            top: this.state.shape.position.height < 0 ? this.state.shape.position.start.y + this.state.shape.position.height : this.state.shape.position.start.y, 
            position: 'absolute',
            display: this.state.shape.visible ? 'inline-block' : 'none',
            color: '#3765AF',
            fontWeight: 'bold'
          }}
          onMouseDown={this.onMouseDown.bind(this)}>
            <span>Object {this.state.shape.index + 1}</span>
        </div>
    )
  }
}

class ShapeControl extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      shape : props.shape,
    }
  }

  render() {
    return(
      <div className="shape-control-section">
        <button className="btn-visibility" onClick={this.changeVisibility.bind(this)}>
          <img className="shape-visibility" src={this.state.shape.visible ? imageShow : imageHide} alt=""/>
        </button>
        <span className="shape-name">OBJ {this.state.shape.index + 1}</span>
      </div>
    )
  }

  changeVisibility = () => {
    this.props.onChangeVisibility(this.state.shape.index);
  }
}

export default class App extends React.Component {

  constructor() {
    super();

    this.state = {
      shapeArry : [],
      isAdding : false,
      shapeCnt : 0,
      pickedPoints : [],
      pointCnt : 0,
      newShape : [],
      dragging : false,
      rel : null,
      byDragging : true
    }
  }

  componentDidUpdate(state) {
    if (this.state.dragging && !state.dragging) {
      document.addEventListener('mousemove', this.onMouseMove);
      document.addEventListener('mouseup', this.onMouseUp);
    } else if (!this.state.dragging && state.dragging) {
      document.removeEventListener('mousemove', this.onMouseMove);
      document.removeEventListener('mouseup', this.onMouseUp);
    }
  }

  onMouseDown = (e) => {
    if (!this.state.isAdding) return;
    if (e.button !== 0) return;
    let pos = ReactDOM.findDOMNode(this);
    let newShape = {
      index : this.state.shapeCnt,
      position : {
        start : {
          x : e.pageX - pos.offsetLeft,
          y : e.pageY - pos.offsetTop,
        },
        width : 0,
        height : 0,
      },
      visible : true
    };

    this.setState({
      newShape: [newShape],
      dragging: true,
      rel: {
        x: e.pageX - pos.offsetLeft,
        y: e.pageY - pos.offsetTop,
      }
    });
    e.stopPropagation();
    e.preventDefault();
  }

  onMouseDown_1 = (e) => {
    if (!this.state.isAdding) return;
    if (e.button !== 0) return;
    let pos = ReactDOM.findDOMNode(this);
    let rectPoints = [...this.state.pickedPoints];
    rectPoints.push({x: e.pageX - pos.offsetLeft, y: e.pageY - pos.offsetTop});
    this.setState({pickedPoints : rectPoints});

    if (rectPoints.length === 4) {
      let rectangle = {
        start : rectPoints[0],
        end : {x: 0, y: 0}
      }
      rectPoints.forEach(pos => {
        rectangle.start.x = rectangle.start.x > pos.x ? pos.x : rectangle.start.x;
        rectangle.start.y = rectangle.start.y > pos.y ? pos.y : rectangle.start.y;
        rectangle.end.x = rectangle.end.x > pos.x ? rectangle.end.x : pos.x;
        rectangle.end.y = rectangle.end.y > pos.y ? rectangle.end.y : pos.y;
      });
      let shapeCnt = this.state.shapeCnt;
      let newShape = {
        index : shapeCnt,
        position : {
          start : rectangle.start,
          width : rectangle.end.x - rectangle.start.x,
          height : rectangle.end.y - rectangle.start.y,
        },
        visible : true
      };
      let shapeArry = [...this.state.shapeArry, newShape];
      this.setState({shapeCnt: shapeCnt + 1});
      this.setState({shapeArry});
      this.setState({pickedPoints: []});
      this.setState({isAdding: false});
      this.setState({dragging: false});
    }

    e.stopPropagation();
    e.preventDefault();
  }

  onMouseUp = (e) => {
    if (!this.state.dragging) return;
    let newShape = {...this.state.newShape[0]};
    let shapeCnt = this.state.shapeCnt;
    newShape.index = shapeCnt;
    if (newShape.position.width < 0) {
      newShape.position.start.x += newShape.position.width;
      newShape.position.width = -newShape.position.width;
    }
    if (newShape.position.height < 0) {
      newShape.position.start.y += newShape.position.height;
      newShape.position.height = -newShape.position.height;
    }
    shapeCnt ++;
    this.setState({shapeArry: [...this.state.shapeArry, newShape]});    
    this.setState({shapeCnt});
    this.setState({newShape : []});
    this.setState({dragging: false});
    this.setState({isAdding: false});
    e.stopPropagation();
    e.preventDefault();
  }
  onMouseMove = (e) => {
    if (!this.state.dragging) return;
    let newShape = {...this.state.newShape[0]};
    console.log('width', e.pageX - this.state.rel.x);
    console.log('height', e.pageY - this.state.rel.y);
    newShape.position.width = e.pageX - this.state.rel.x;
    newShape.position.height = e.pageY - this.state.rel.y;
    this.setState({ newShape : [newShape] })
    e.stopPropagation();
    e.preventDefault();
  }

  render() {
    return(
      <div className="test-app">
        <div className="image-board-section" onMouseDown={this.state.byDragging ? this.onMouseDown.bind(this) : this.onMouseDown_1.bind(this)} style={{cursor: this.state.isAdding ? 'copy' : 'default' }}>
          <span>{this.state.byDragging ? 'Select rectangle area by dragging' : 'Select rectangle area by putting 4 dots'}</span>
          {
            this.state.shapeArry.map((item, index) => (
              <ShapeView onChangePosition={this.onChangePosition} key={index} shape={item}/>
            ))
          }
          {
            this.state.pickedPoints.map((item, index) => (
              <div style={{backgroundColor:'red', width: 3, height: 3, position:'absolute', left:item.x, top:item.y}} key={index}></div>
            ))
          }
          {
            this.state.newShape.map((item, index) => (
              <ShapeView onChangePosition={this.onChangePosition} key={index} shape={item}/>
            ))
          }
        </div>
        <div className="controller-section">
          <div className="add-new-shape">
            <button className="btn-new-shape" onClick={() => this.setState({isAdding: true})} style={{backgroundColor : this.state.isAdding ? '#DDDDDD' : '#FFFFFF'}}>
              + Add New
            </button>
            <p>Select Rectangle by</p>
            <div>
              <input type="radio" checked={this.state.byDragging === true} onChange={this.onChangeMode} /> <span>Dragging</span>
            </div>
            <div>
              <input type="radio" checked={this.state.byDragging === false} onChange={this.onChangeMode} /> <span>4 dots</span>
            </div>            
          </div>
          <div className="shape-list">
            {
              this.state.shapeArry.map((item, index) => (
                <ShapeControl onChangeVisibility={this.onChangeVisibility.bind(this)} key={index} shape={item}/>
              ))
            }
          </div>
        </div>
      </div>
    )
  }

  onChangeMode = () => {
    this.setState({byDragging: !this.state.byDragging})
  }

  onChangePosition = (changedPosition, index) => {
    let shapeArry = this.state.shapeArry;
    shapeArry[index].position.start = changedPosition;
    this.setState({shapeArry});
  }

  onChangeVisibility = (index) => {
    let shapes = this.state.shapeArry;
    shapes[index].visible = !shapes[index].visible;
    this.setState({shapeArry: shapes});
  }
}
